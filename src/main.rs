use actix_web::{web, App, HttpServer, middleware, HttpResponse, Error, error};
use std::sync::{RwLock, Arc};

#[macro_use]
extern crate tera;

struct Counter {
    value: i64,
}

impl Counter {
    pub fn new() -> Self {
        Counter {
            value: 0,
        }
    }

    pub fn increment(&mut self) {
        self.value = self.value + 1;
    }

    pub fn decrement(&mut self) {
        self.value = self.value - 1;
    }

    pub fn value(&self) -> i64 {
        self.value
    }
}

fn index(tmpl: web::Data<tera::Tera>, counter: web::Data<Arc<RwLock<Counter>>>) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();
    ctx.insert("value", &counter.read().unwrap().value());

    let s = tmpl.render("index.html", &ctx).map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn increment(tmpl: web::Data<tera::Tera>, counter: web::Data<Arc<RwLock<Counter>>>) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();
    counter.write().unwrap().increment();
    
    ctx.insert("value", &counter.read().unwrap().value());

    let s = tmpl.render("index.html", &ctx).map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn decrement(tmpl: web::Data<tera::Tera>, counter: web::Data<Arc<RwLock<Counter>>>) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();
    counter.write().unwrap().decrement();
    
    ctx.insert("value", &counter.read().unwrap().value());

    let s = tmpl.render("index.html", &ctx).map_err(|_| error::ErrorInternalServerError("Template error"))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    
    let counter = Arc::new(RwLock::new(Counter::new()));

    HttpServer::new(move || {
        let tera =
            compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*"));

        App::new()
            .data(tera)
            .wrap(middleware::Logger::default()) // enable logger
            .data(counter.clone())
            .service(web::resource("/").route(web::get().to(index)))
            .service(web::resource("/increment").route(web::get().to(increment)))
            .service(web::resource("/decrement").route(web::get().to(decrement)))
        })
    .bind("0.0.0.0:8000")?
    .run()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_increment_once() {
        let mut counter = Counter::new();

        counter.increment();

        assert_eq!(counter.value(), 1);
    }

    #[test]
    fn test_increment_twice() {
        let mut counter = Counter::new();

        counter.increment();
        counter.increment();
        

        assert_eq!(counter.value(), 2);
    }

    #[test]
    fn test_increment_thrice() {
        let mut counter = Counter::new();

        counter.increment();
        counter.increment();
        counter.increment();

        assert_eq!(counter.value(), 3);
    }

    #[test]
    fn test_decrement_once() {
        let mut counter = Counter::new();

        counter.decrement();

        assert_eq!(counter.value(), -1);
    }

    #[test]
    fn test_decrement_twice() {
        let mut counter = Counter::new();

        counter.decrement();
        counter.decrement();
        

        assert_eq!(counter.value(), -2);
    }
    
    #[test]
    fn test_decrement_thrice() {
        let mut counter = Counter::new();

        counter.decrement();
        counter.decrement();
        counter.decrement();

        assert_eq!(counter.value(), -3);
    }

}